kdeplasma-addons (4:5.19.5-5) UNRELEASED; urgency=medium

  [ John Scott ]
  * Suggest the quota package needed for the Disk Quota desktop widget.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Sun, 29 Nov 2020 17:42:06 -0500

kdeplasma-addons (4:5.19.5-4) unstable; urgency=medium

  [ Scarlett Moore ]
  * Remove obsolete entries in d/upstream/metadata already
    defined in d/copyright.
  * Add Multi-Arch according to multiarch hints.
  * Tighten runtime dependency versions.

 -- Norbert Preining <norbert@preining.info>  Fri, 06 Nov 2020 08:37:22 +0900

kdeplasma-addons (4:5.19.5-3) experimental; urgency=medium

  * Rebuild for Qt 5.15

 -- Norbert Preining <norbert@preining.info>  Mon, 02 Nov 2020 09:27:39 +0900

kdeplasma-addons (4:5.19.5-2) experimental; urgency=medium

  * Fix FTBFS on non-QtWebEngine platforms.

 -- Norbert Preining <norbert@preining.info>  Fri, 23 Oct 2020 10:03:11 +0900

kdeplasma-addons (4:5.19.5-1) experimental; urgency=medium

  * New upstream release (5.19.5).
  * Add Patrick and myself to uploaders.

 -- Norbert Preining <norbert@preining.info>  Mon, 19 Oct 2020 10:36:33 +0900

kdeplasma-addons (4:5.19.4-1) experimental; urgency=medium

  * Team upload.

  [ Scarlett Moore ]
  * Bump compat level to 13.
  * Add Rules-Requires-Root field to control.
  * New upstream release (5.18.5).
  * Update build-deps and deps with the info from cmake.
  * Refresh install file for 3 new addons:
    dict, nightcolorcontrol, webbrowser.
  * Delete not needed Breaks/Confilcts.
  * Add myself to Uploaders.
  * Remove not needed injection of linker flags.
  * Update Homepage link to point to new invent.kde.org
  * Update field Source in debian/copyright to invent.kde.org move.
  * Set/Update field Upstream-Contact in debian/copyright.

  [ Patrick Franz ]
  * Add hardening=+all build flag.
  * Update matching files in debian/copyright.
  * Update data in /debian/upstream/metadata.
  * New upstream version (5.19.4).
  * Bump Qt build-dependencies to 5.14.
  * Bump KF5 build-dependencies to 5.70.
  * Change Maintainer in debian/control to Debian Qt/KDE Maintainers.
  * Remove Maxy from Uploaders as requested.

 -- Norbert Preining <norbert@preining.info>  Fri, 16 Oct 2020 10:30:54 +0900

kdeplasma-addons (4:5.17.5-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Bump Standards-Version to 4.5.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 14 Feb 2020 21:12:20 +0100

kdeplasma-addons (4:5.17.5-1) experimental; urgency=medium

  * Team upload.

  [ Maximiliano Curia ]
  * New upstream release (5.16.5).
  * Update build-deps and deps with the info from cmake
  * Salsa CI automatic initialization by Tuco

  [ Pino Toscano ]
  * New upstream release.
  * Update the patches:
    - upstream_Fix-build.patch: drop, backported from upstream
  * Update the build dependencies according to the upstream build system:
    - remove unused: kinit-dev, libboost-dev, libgee-0.8-dev, libglib2.0-dev,
      libibus-1.0-dev, libkf5activities-dev, libkf5configwidgets-dev,
      libkf5itemmodels-dev, libkf5kdelibs4support-dev, libkf5sysguard-dev,
      libqt5x11extras5-dev, libscim-dev, libx11-dev, libxcb-image0-dev,
      libxcb-keysyms1-dev, libxcb-shm0-dev, libxcb1-dev, plasma-workspace-dev,
      and shared-mime-info (Closes: #661366)
  * Drop the kde-l10n breaks/replaces, no more needed after two Debian stable
    releases.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Update install files.
  * Unregister old conffiles.

 -- Pino Toscano <pino@debian.org>  Sat, 18 Jan 2020 19:22:44 +0100

kdeplasma-addons (4:5.14.5.1-2) unstable; urgency=medium

  * Team upload.

  [ John Scott ]
  * Add dependency on qml-module-qtwebengine (Closes: 925406)

  [ Pino Toscano ]
  * Bump Standards-Version to 4.4.1, no changes required.
  * Pass -DBUILD_TESTING=OFF to cmake to disable the build of tests, as they
    are not run at build time anyway.
  * Backport upstream commit 959443f85ec94d0748d536a07edfa5a68c7d254b to fix
    build with newer KF5; patch upstream_Fix-build.patch.
  * Explicitly add the gettext build dependency.
  * Drop the migration from kdeplasma-addons-dbg, no more needed after two
    Debian stable releases.
  * Do not use the ${shlibs:Depends} substvar for kdeplasma-addons-data, as
    it is an arch:all package.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Thu, 03 Oct 2019 07:48:46 +0200

kdeplasma-addons (4:5.14.5.1-1) unstable; urgency=medium

  * New upstream bugfix release (5.14.5.1).
    - [weather] Center align the warning heading (fba8a13f)
    - [weather] Fix weather Notices tab not showing (46204a5e)
    - Translation updates (556106bd, dbf8de65, 97c86def)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 13 Feb 2019 19:39:25 +0100

kdeplasma-addons (4:5.14.5-1) unstable; urgency=medium

  * New upstream release (5.14.5).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 24 Jan 2019 09:25:46 -0300

kdeplasma-addons (4:5.14.3-1) unstable; urgency=medium

  * Update upsteam signing-key
  * New upstream release (5.14.3).
  * Update build-deps and deps with the info from cmake
  * Bump group breaks (4:5.14)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 23 Nov 2018 08:50:55 -0300

kdeplasma-addons (4:5.13.5-1) unstable; urgency=medium

  * New upstream release (5.13.5).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 06 Sep 2018 20:40:31 +0200

kdeplasma-addons (4:5.13.4-1) unstable; urgency=medium

  * New upstream release (5.13.4).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sun, 19 Aug 2018 23:17:56 +0200

kdeplasma-addons (4:5.13.2-1) unstable; urgency=medium

  * New upstream release (5.13.2).
  * Add new binary package for the calendar addons
  * Update the not-installed file
  * Update the addons listed in the package descriptions
  * Update copyright information
  * Drop old not-installed entries
  * Install the webbrowser applet only when built
  * Drop special lancelot clean rule
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Mon, 02 Jul 2018 14:10:50 +0200

kdeplasma-addons (4:5.13.1-1) unstable; urgency=medium

  * New revision
  * Add recommends for ksysguard (Closes: 900523)
  * Use libscim-dev build dependency.
    Thanks to Helmut Grohne for the report (Closes: 893441)
  * New upstream release (5.13.1).
  * Update build-deps and deps with the info from cmake
  * Update install files
  * Bump group breaks (4:5.13)
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Tue, 26 Jun 2018 13:42:55 +0200

kdeplasma-addons (4:5.12.5-1) unstable; urgency=medium

  * New upstream release (5.12.5).
  * Bump Standards-Version to 4.1.4.
  * Use https for the debian/copyright
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 09 May 2018 13:23:55 +0200

kdeplasma-addons (4:5.12.4-1) unstable; urgency=medium

  * New upstream release (5.12.4).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 28 Mar 2018 18:12:41 +0200

kdeplasma-addons (4:5.12.3-1) sid; urgency=medium

  * New upstream release (5.12.3).
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Wed, 07 Mar 2018 19:14:04 +0100

kdeplasma-addons (4:5.12.2-1) sid; urgency=medium

  * New upstream release (5.12.2).
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sat, 24 Feb 2018 09:47:51 +0100

kdeplasma-addons (4:5.12.1-1) sid; urgency=medium

  * Use the salsa canonical urls
  * New upstream release (5.12.1).
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Tue, 20 Feb 2018 22:08:44 +0100

kdeplasma-addons (4:5.12.0-3) sid; urgency=medium

  * New revision
  * Limit the optional qtwebengine dependency to the arches where it's present
  * Install the dict applet only if we could build it
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Wed, 14 Feb 2018 21:42:07 +0100

kdeplasma-addons (4:5.12.0-2) sid; urgency=medium

  * New revision
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Mon, 12 Feb 2018 16:03:31 +0100

kdeplasma-addons (4:5.12.0-1) experimental; urgency=medium

  * Bump debhelper build-dep and compat to 11.
  * Build without build_stamp
  * Add link options as-needed
  * Add Bhushan Shah upstream signing key
  * New upstream release (5.12.0).
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.1.3.
  * Bump itemmodels build dep version
  * Update install files
  * Add qtwebengine5-dev for the dict applet
  * Use https uri for uscan
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Thu, 08 Feb 2018 15:20:35 +0100

kdeplasma-addons (4:5.11.4-1) experimental; urgency=medium

  * New upstream release (5.11.4).
  * Bump Standards-Version to 4.1.2.
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Wed, 03 Jan 2018 16:48:50 -0300

kdeplasma-addons (4:5.10.5-2) sid; urgency=medium

  * New revision
  * Bump Standards-Version to 4.1.0.
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sun, 03 Sep 2017 09:55:18 +0200

kdeplasma-addons (4:5.10.5-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * Update plasma-wallpapers-addons package description.
    Thanks to Ralf Jung for reporting (Closes: 861318)
  * New upstream release (5.10.3).
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.0.0.
  * Update upstream metadata
  * New upstream release (5.10.4).
  * Update build-deps and deps with the info from cmake
  * Update install file
  * New upstream release (5.10.5).
  * Release to experimental

  [ Jonathan Riddell ]
  * update watch file to version=4 with pgp signature checking
  * more qml deps
  * qmllint
  * update .install
  * update .install
  * update .install
  * appstream
  * update .install file
  * update not-installed

  [ Clive Johnston ]
  * Wildcarding /usr/share/locale/*/*/*.mo in install file

  [ Rik Mills ]
  * Install files for binary clock applet

 -- Maximiliano Curia <maxy@debian.org>  Mon, 28 Aug 2017 15:29:35 +0200

kdeplasma-addons (4:5.8.5-2) unstable; urgency=medium

  * Release to unstable.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Mar 2017 08:50:49 +0100

kdeplasma-addons (4:5.8.5-1) experimental; urgency=medium

  * New upstream release (5.8.5).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 30 Dec 2016 18:46:14 +0100

kdeplasma-addons (4:5.8.4-1) unstable; urgency=medium

  * New upstream release (5.8.4)
  * Add the quickshare plasmoid

 -- Maximiliano Curia <maxy@debian.org>  Wed, 23 Nov 2016 18:35:21 +0100

kdeplasma-addons (4:5.8.2-1) unstable; urgency=medium

  * New upstream release (5.8.2)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 19 Oct 2016 15:27:38 +0200

kdeplasma-addons (4:5.8.1-1) unstable; urgency=medium

  * New upstream release (5.8.1)
  * Add quickshare to not-installed till purpose is available

 -- Maximiliano Curia <maxy@debian.org>  Sun, 16 Oct 2016 23:01:34 +0200

kdeplasma-addons (4:5.8.0-1) unstable; urgency=medium

  * New upstream release (5.8.0)
  * Update install files
  * Add new plasmoid (minimizeall)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 07 Oct 2016 14:10:26 +0200

kdeplasma-addons (4:5.7.4-1) unstable; urgency=medium

  * New upstream release (5.7.4)
  * Bump plasma-workspace-dev build dependency version
  * Update install files

 -- Maximiliano Curia <maxy@debian.org>  Wed, 31 Aug 2016 14:16:59 +0200

kdeplasma-addons (4:5.6.5-1) unstable; urgency=medium

  * New upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 28 Jun 2016 14:53:12 +0200

kdeplasma-addons (4:5.6.4-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Bump Standards-Version to 3.9.8

 -- Maximiliano Curia <maxy@debian.org>  Tue, 31 May 2016 21:28:33 +0200

kdeplasma-addons (4:5.6.2-1) unstable; urgency=medium

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

  [ Maximiliano Curia ]
  * uscan no longer supports this kind of watch files.
  * New upstream release (5.5.5).
  * Automatic update with ddeb_migration3.py
  * Add upstream metadata (DEP-12)
  * Update build-depends
  * Add libksysguard as a build dep
  * Drop kimpanel package
  * Update install files
  * debian/control: Update Vcs-Browser and Vcs-Git fields
  * Remove duplicated build dependency

 -- Maximiliano Curia <maxy@debian.org>  Tue, 31 May 2016 21:23:09 +0200

kdeplasma-addons (4:5.5.4-1) experimental; urgency=medium

  * New upstream release (5.5.0).
  * New upstream release (5.5.1).
  * New upstream release (5.5.2).
  * New upstream release (5.5.3).
  * New upstream release (5.5.4).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 16:49:09 +0100

kdeplasma-addons (4:5.4.3-0ubuntu1) xenial; urgency=medium

  [ Scarlett Clark ]
  * New upstream bugfix release

  [ Philip Muškovac ]
  * New upstream bugfix release (LP: #1518598)

 -- Philip Muškovac <yofel@kubuntu.org>  Sun, 22 Nov 2015 16:29:09 +0100

kdeplasma-addons (4:5.4.2-0ubuntu1) wily; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Fri, 02 Oct 2015 15:52:40 +0100

kdeplasma-addons (4:5.4.3-1) unstable; urgency=medium

  * New upstream release (5.4.3).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 01 Dec 2015 11:46:01 +0100

kdeplasma-addons (4:5.4.2-1) unstable; urgency=medium

  * New upstream release (5.4.2).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 06 Oct 2015 07:52:17 +0200

kde-gtk-config (4:5.4.2-0ubuntu1) wily; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Fri, 02 Oct 2015 15:52:02 +0100

kdeplasma-addons (4:5.4.1-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.4.1).

  [ Scott Kitterman ]
  * Drop unused build-depends on libgee-dev
  * Add kwin-x11 as a real alternative to the kwin virtual package depends for
    kwin-addons to make the package more installable (Closes: #791381)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 11 Sep 2015 18:45:32 +0200

kdeplasma-addons (4:5.4.1-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Tue, 08 Sep 2015 10:14:40 +0100

kdeplasma-addons (4:5.4.0-1) unstable; urgency=medium

  * New upstream release (5.4.0).
  * Update install files.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 03 Sep 2015 18:00:02 +0200

kdeplasma-addons (4:5.4.0-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 31 Aug 2015 15:48:32 +0100

kdeplasma-addons (4:5.3.95-0ubuntu2) wily; urgency=medium

  * Build-depend on unversioned boost package.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 28 Aug 2015 13:58:26 +0200

kdeplasma-addons (4:5.3.95-0ubuntu1) wily; urgency=medium

  * new upstream beta release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 10 Aug 2015 23:12:41 +0200

kdeplasma-addons (4:5.3.2-2) unstable; urgency=medium

  * Add the missing Breaks/Replaces. (Closes: #790919)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 03 Jul 2015 14:07:47 +0200

kdeplasma-addons (4:5.3.2-1) unstable; urgency=medium

  * New upstream release (5.3.2).
  * Update copyright information.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 30 Jun 2015 20:36:27 +0200

kdeplasma-addons (4:5.3.2-0ubuntu1) wily; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Thu, 02 Jul 2015 12:26:53 +0000

kdeplasma-addons (4:5.3.1-1) unstable; urgency=medium

  * New upstream release (5.3.0).
  * New upstream release (5.3.1).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 29 Jun 2015 23:44:54 +0200

kdeplasma-addons (4:5.3.1-0ubuntu1) wily; urgency=medium

  [ Jonathan Riddell ]
  * Plasma 5.3 beta
  * new upstream release

  [ Scarlett Clark ]
  * Vivid backport

  [ Jonathan Riddell ]
  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 05 Jun 2015 02:27:29 +0200

kdeplasma-addons (4:5.2.2-1) experimental; urgency=medium

  * New upstream release (5.2.1).
  * New upstream release (5.2.2).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 25 Mar 2015 23:17:49 +0100

kdeplasma-addons (4:5.2.2-0ubuntu3) vivid; urgency=medium

  * Add depends on qml-module-qtwebkit LP: #1431610
    Web Browser plasmoid missing dependency to QtWebKit

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 15 Apr 2015 12:34:59 +0200

kdeplasma-addons (4:5.2.2-0ubuntu2) vivid; urgency=medium

  * Fix .install files

 -- Jonathan Riddell <jriddell@ubuntu.com>  Wed, 25 Mar 2015 00:49:53 +0100

kdeplasma-addons (4:5.2.2-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Tue, 24 Mar 2015 07:13:42 -0700

kdeplasma-addons (4:5.2.1-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 23 Feb 2015 09:37:47 -0800

kdeplasma-addons (4:5.2.0-1) experimental; urgency=medium

  * Prepare initial Debian release.
  * Add myself as Uploader.
  * Update build dependencies to build against experimental and to
    follow cmake.
  * Update copyright information.
  * Update install files.
  * Update watch file.

 -- Maximiliano Curia <maxy@debian.org>  Mon, 09 Feb 2015 13:48:48 +0100

kdeplasma-addons (4:5.2.0-0ubuntu1) vivid; urgency=medium

  * Fix the -data depends to use source:Version rather than binary:
  * New upstream release

 -- Harald Sitter <sitter@kde.org>  Tue, 27 Jan 2015 14:51:11 +0100

kdeplasma-addons (4:5.1.95-0ubuntu2) vivid; urgency=medium

  * Make packages depend on the -data package for translations

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 16 Jan 2015 12:01:39 +0100

kdeplasma-addons (4:5.1.95-0ubuntu1) vivid; urgency=medium

  * New upstream beta release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Thu, 15 Jan 2015 01:25:22 +0100

kdeplasma-addons (4:5.1.2-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 19 Dec 2014 18:12:50 +0100

kdeplasma-addons (4:5.1.1-0ubuntu1) vivid; urgency=medium

  * Initial release for Plasma 5
  * Remove no-human-maintainers lintian-override per ScottK.
  * New upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Mon, 10 Nov 2014 21:19:52 +0100

kdeplasma-addons (4:5.1.0.1-0ubuntu2) vivid; urgency=medium

  * Remove unused build-depend libgio3.0-cil-dev

 -- Jonathan Riddell <jriddell@ubuntu.com>  Thu, 06 Nov 2014 11:31:26 +0100

kdeplasma-addons (4:5.1.0.1-0ubuntu1) vivid; urgency=medium

  * Initial release for Plasma 5
  * New copyright file, wrap-and-sort, changelog entry, add epoch.
  * Fix Maintainer field.
  * New upstream tarball, add new dependencies.
  * Correct the xcb dependency.

 -- Scarlett Clark <sgclark@kubuntu.org>  Fri, 03 Oct 2014 10:59:34 +0000
